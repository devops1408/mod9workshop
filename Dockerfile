FROM mcr.microsoft.com/dotnet/sdk:6.0
RUN curl -fsSL https://deb.nodesource.com/setup_19.x | sudo -E bash - &&\
COPY ./dotnetapp
WORKDIR /dotnetapp
RUN dotnet build
WORKDIR /dotnetapp/DotnetTemplate.Web
RUN npm install
RUN npm build
ENTRYPOINT cd /dotnetapp/DotnetTemplate.Web && dotnet run